
const mongoose = require('mongoose')

mongoose.connect(process.env.MONGODB_URL , { monitorCommands : true ,
                                             maxPoolSize : process.env.MAX_POOL_SIZE})
        .then (connection => 
            console.log('SERVER CONNECTED TO DATABASE MANAGEMENT SYSTEM SUCCESSFULLY ') )
        .catch ( error=> console.error( error ))
