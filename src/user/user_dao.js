const { default: mongoose } = require('mongoose');
const User = require('../../database/user');

exports.registerUser = ( newUser) =>{

        const user = new User( newUser)
        user.save()
        console.log(` user added :>> `);
        console.log({ ...user });
        return user
}

// returns the result as an array
exports.getUserByUsername = ( username ) => {
 
        return User.find(
            {
                username : username
            }
        ) 
}

exports.getUserByPassword = ( userToLogged ) => {

    let { passwd } = userToLogged 
    return User.find(
        {
            password : passwd
        }
    ) 

}

exports.getUserById = ( userid ) => {

    return User.findOne(
        {
            _id : mongoose.Types.ObjectId(userid)
        }
    ) 

}