const user_dao = require('./user_dao')
const Error = require('../commons/Error/exceptions')
const cryptoutili = require('../commons/cryptoutil')
exports.registerUser =async ( user ) =>{

    let {username , password ,role} = user
    if( !username || !password || !role){
        throw new Error.InvalidUserInputs(` MISSED USER'S INPUT` )
    }
    let exists =await user_dao.getUserByUsername(username)
    // exists = exists.toJSON()

    console.log(` object :>> `);
    console.log({ exists });

    if(exists[0]){
      throw new Error.InvalidUserInputs('The username inuse')
    }else{
        //crypt the password
    let hash_salt_obj = cryptoutili.getHashAndSalt(password)

    let user_toa = {
        username : username ,
        password : hash_salt_obj.hashedPassword ,
        salt     : hash_salt_obj.salt ,
        role     : role
                    }
    return user_dao.registerUser(user_toa)
    }
}

exports.loginUser = async ( userCredantials ) =>{

    let {username , password } = userCredantials
    let user = await user_dao.getUserByUsername(username)
    if(!user){
        throw new Error.InvalidUserInputs(`The username doesn't exists`)
    }
    let database_hashed = user[0].password
    let user_salt       = user[0].salt
    let passed_password = cryptoutili.getHash(password , user_salt )
    let login
    if( database_hashed == passed_password ){
        login = user
    } else {
        login = false          
    }

    return Promise.resolve(login)
}

exports.getUserById = (userId)=>{

    return user_dao.getUserById(userId)

}


