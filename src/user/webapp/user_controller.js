
const user_services = require('../user_services')
const Error = require('../../commons/Error/exceptions')
exports.registerUser = async ( req , res ) =>{ 

    try{

        let user = { 
            username : req.body.username ? req.body.username : null  ,
            password : req.body.password ? req.body.password : null  ,
            role : req.body.role ? req.body.role : null ,
        }
      
        let reg_status = await user_services.registerUser(user)

        res.send(`${reg_status}`)

    }catch(e){
    res.send(e)
    }
}

exports.loginUser = async ( req , res ) =>{

    try{
        let userCredantials = { 
            username : req.body.username ? req.body.username : null ,
            password : req.body.password ? req.body.password : null 
        }
        
        let user = await user_services.loginUser(userCredantials)

        if(user){
            res.json({ msg : `LOGGED IN SUCCESSFULLY`})
        }else{
            res.json({ msg : `LOGGED IN FAILED`})          
        }
    } catch(e){
        res.send(e)
    }

}

exports.getRegisterForm = ( req , res ) => {

        let form =  `<form action="/register">
                    <p>
                        <label for="username"> Username</label>
                        <input type="text">
                    </p>
                    <p>
                        <label for="password"> Password </label>
                        <input type="password">
                    </p>
                    
                    </form>`

    res.send( form )


}

exports.logout = (req , res , next ) =>{ 
 
    req.session ?
    (req.session.destroy() , res.send(`YOU ARE SIGNED OUT`))  :
    ( res.send('u r out ') )

}
