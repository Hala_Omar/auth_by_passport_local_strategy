const LocalStrategy = require('passport-local').Strategy
const req = require('express/lib/request')
const user_services  = require('../../user/user_services')
const { InvalidUserInputs } = require('../Error/exceptions')

const local_strategy = new LocalStrategy(verify)

async function verify(username , password , done){

    try{

        let userCredientials = { username , password }

        let user = await user_services.loginUser(userCredientials)
        
        if(user[0]){
             done(null , user[0])
            
        } else {
            return done(null, false)
                    
        } }catch(e){

        }

}

module.exports = function configPassport( passport) {
    
    passport.use(local_strategy)
    passport.serializeUser( (user , done)=>{
        console.log(`SERIALIZATION USER BY user_id ${user._id} :>>` );
        done(null , user._id )})
    passport.deserializeUser( (userId , done )=>{

        user_services.getUserById(userId).then((user,error)=>{ done(error,user) })
        return
    })
}
