const Error = require('../Error/exceptions')
const passport = require('passport')
const  path  = require('path')

exports.authenticate = function(req, res , next){
    
try{
    let cb =  function( error , user ) {

        if( error || user == false){
          return  res.status(401).send({
                msg : 'YOU ARE NOT AUTHENTICATED'
            })
        }
        
        req.user = user 
        req.session.passport={}
        req.session.passport.user =  {id :user._id}
        next()
    }   

        passport.authenticate('local' , 
        { successRedirect:`profile`})(req,res,next)

}catch(e){
    throw e
}   
}

exports.isAuth = ( req , res , next ) =>{
    //you can check with req.user also 
    req.isAuthenticated() ? 
    next()                : 
    res.status(401).json({
        msg : `YOU ARE NOT AUTHORIZED TO ACCESS THIS RESOURSE`
    })
}

exports.isAdminOrSupervisor = ( req , res , next ) =>{

    let { user } = req
    let { role } = user
    // if(req.isAuthenticated() && user.role =='admin')
    if (user.role == 'admin' || user.role == 'supervisor') {

         next()
    } else {
        console.log(` user.role :>> ${role}`);
        throw new Error.AuthorizationFail("YOU ARE NOT AUTHORIZED")
    }
}

