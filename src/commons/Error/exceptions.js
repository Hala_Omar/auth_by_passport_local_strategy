class InvalidUserInputs {
    constructor(message){
        this.errormsg = message
        this.status = 400
    }
}

class AuthorizationFail{
    constructor(message){
        this.message = message , 
        this.status  = 401
    }
}

module.exports ={
    InvalidUserInputs ,
    AuthorizationFail
}

