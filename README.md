**#connect-mongodb-session**

MongoDB-backed session storage for connect and Express.

MongoDBStore

This module exports a single function which takes an instance of connect (or Express) and #returns a MongoDBStore class that can be used to store sessions in MongoDB.

If you pass in an instance of the express-session module the MongoDBStore class will enable you #to store your Express sessions in MongoDB.

_The MongoDBStore class has 3 required options:_

1.  uri: a MongoDB connection string

2.  databaseName: the MongoDB database to store sessions in

3.  collection: the MongoDB collection to store sessions in

**######################## Passport ################################**
serializeUser is called and set the passport property on the 

session object req.session.passport.user={id:user._id} :>> 

deserializeUser called and read the id property from the req.session.passport.user.id

and call the function on the id to get the user then the done function will set the 

user property on the req like req.user = user 

passport.session() will call the deserializeUser

req.logout() will delete req.session.passport.user property

isAuthenticated() check if the user property is set on the req.session.passport

req.isAuthenticated() and req.user are the same

the passport middlewares will access only the req object based on the session object

that populated by the session middleware previouslly


**####################### session Middleware ######################**
************

this middleware is responsible of setting the req.sessionID and the req.session 

the middleware will read the session_id from the sent cookie and search in the #sessionStore, if exixts the req.sessionId is set and the req.session is populated 

from the store as json object then passport will read the user property from passport

but if not the genid() function will be called and new session object is created 

**######################## JWT Authentication #####################**

pre-requirement to it is to have good knowledge about Public-key cryptography

public key cryptography in abstract is to encrypt data by the public key and 

decrypt by the private key

It is not possible to derive the data from the hashed word, having the private key you #can got the public key but vise verse can not, in other word you can share the public #key with all but private key not  
