const express = require('express')

require('dotenv').config({ path: `${__dirname}/.env` })


const session = require('express-session')
const connectDB = require('./confg/database')
const MongoStore = require('connect-mongodb-session')(session)
const passport = require('passport')

require('./src/commons/authcontroller/passportconfig')(passport)

const app = express()

const storeConfig = { uri: process.env.MONGODB_URL , collection :'sessions'}


const sessionStore = new MongoStore(storeConfig)

app.use(session({
    cookie :{
        maxAge :  60 * 60 * 1000 ,
        httpOnly : true} ,
    name   : process.env.SESSION_NAME ,
    secret : process.env.SESSION_SECRET ,
    store  : sessionStore , 
    resave : true ,
    saveUninitialized:true
}))


app.use(express.json())
app.use(express.urlencoded({extended :true}))
app.use(passport.initialize())
app.use(passport.session())

app.use((req , res , next ) =>{
    !req.session && console.log('req.session :>> ', req.session);
    next()
})


app.use('/api/v1/user', require('./src/user/webapp/user_router'))


// Error Handler
app.use((error , req , res , next)=>{
    res.json(error)
})


app.listen(process.env.PORT , ()=>{
    console.log(`Request Listener listens at port ${process.env.PORT}`);
})